# TELLEM

a dead-simple audio level monitor

Depends on some kind of `jack` setup (I use pipewire's jack interface)

## Why

I got tired of saying "Hey am I too quiet? Hey are my levels good? Can you hear me?"

Madness.

## Usage

### build

```sh
# I put it directly in my local executables directory
cargo build -Z unstable-options --out-dir ~/.local/bin --release

# or you can just build it
cargo build --release

# or you can just run it
cargo run
```

### what it does
`tellem`

No arguments. _Ever_.

Draws a row of `#` depending on the highest absolute level received on the
audio input interface per tick. If this level is greater than 1, attempts to
draw in red (to indicate clipping).
