use console::{Color, Style, Term};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
// use cpal::Data;
// use cpal::{Host, HostId};
use std::thread;
use std::time;

fn main() {
    let term = Term::stdout();
    let (_, cols) = term.size();
    let color_red = Style::new().fg(Color::Red);

    let hosts = cpal::available_hosts();
    let jack_id = hosts
        .into_iter()
        .filter(|x| x.name().to_lowercase() == "jack")
        .collect::<Vec<_>>()
        .first()
        .expect("Should have found the Jack host")
        .clone();

    let host = cpal::host_from_id(jack_id).expect("Could not get Jack host");

    // let device = host
    //     .default_input_device()
    //     .expect("Not input device available");
    //
    // let mut supported_configs_range = device
    //     .supported_output_configs()
    //     .expect("Error whily querying configs");
    //
    // let supported_config = supported_configs_range.next()
    //     .expect("No supported config wtf")
    //     .with_max_sample_rate();
    //
    // println!("{:?}", supported_config);
    //
    // let ostream = device.build_output_stream(
    //     &supported_config.config(),
    //     move |data: & mut [f32], _| {
    //         // react to stream events and read or write stream data here
    //     },
    //     move |err| {
    //         // react to errors here
    //     }, None // None = blocking, Some(Duration) = timeout
    // ).expect("Should have created stream");
    let device = host
        .default_input_device()
        .expect("Not output device available");

    let mut supported_configs_range = device
        .supported_input_configs()
        .expect("Error whily querying configs");

    let supported_config = supported_configs_range
        .next()
        .expect("No supported config wtf")
        .with_max_sample_rate();

    println!("{:?}", supported_config);

    let stream = device
        .build_input_stream(
            &supported_config.config(),
            move |data: &[f32], _| {
                // react to stream events and read or write stream data here
                // get abs low/high
                let (_, maxim) = data
                    .into_iter()
                    .map(|x| x.abs())
                    .fold((f32::MAX, f32::MIN), |curr, x| {
                        (curr.0.min(x), curr.1.max(x))
                    });
                // let mini_cols = (cols as f32 * mini).floor() as usize;
                let maxim_cols = (cols as f32 * maxim).floor() as usize;
                // let mini_draw = String::from_iter([0..mini_cols].map(|_| '#'));
                // let maxim_draw = String::from_iter([0..maxim_cols].map(|_| '#'));
                // let mini_draw = std::iter::repeat('#').take(mini_cols).collect::<String>();
                //term.write_line(mini_draw.as_str());

                // clipping logic
                let should_clip = maxim >= 1.0;

                let maxim_draw = std::iter::repeat('#')
                    .take(maxim_cols.min(cols.into()))
                    .collect::<String>();
                term.clear_screen().expect("Should have cleared");
                //println!("min: {}\nmax: {}", mini, maxim);
                if should_clip && console::colors_enabled() {
                    //let styled = s.apply_to(term.clone());
                    let styled = color_red.apply_to(maxim_draw);
                    println!("{}", styled);
                } else {
                    term.write_line(maxim_draw.as_str())
                        .expect("Should have drawn");
                }
                term.move_cursor_to(0,0).expect("shoulda moved");
            },
            move |_| {
                // react to errors here
            },
            None, // None = blocking, Some(Duration) = timeout
        )
        .expect("Should have created stream");

    stream.play().unwrap();
    loop {
        // just busyloop forever ig
        // Theoretically I could handle errors by listening on something here
        // and sending to it from my error callback?
        thread::sleep(time::Duration::from_secs(1));
    }
}
